import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import axios from 'axios';
import QuestionList from '../question-list/QuestionList';
import SurveyList from '../survey-list/SurveyList';

class App extends Component {
  constructor() {
    super();
    this.state = {
      questions: null,
      activeQuestion: null,
      surveys: [],
      displayedSurveys: []
    };
  }

  componentDidMount() {
    axios.get(`http://localhost:8080/surveys`)
      .then(res => {
        const surveys = res.data;
        this.setState({ surveys });
        this.filterSurveys("");
      });
  }

  getQuestions(code) {
    const activeQuestion = code;
    this.setState({ activeQuestion });    
    axios.get(`http://localhost:8080/surveys/${code}`)
    .then(res => {
      const questions = res.data;
      this.setState({ questions });
    }, error => console.log(error));
  }

  filterSurveys(key) {
    if (key && key.length) {
      const displayedSurveys = this.state.surveys.filter((survey) => {
        return survey.name.indexOf(key)>-1 || survey.code.indexOf(key)>-1
      });
      this.setState({ displayedSurveys });
    }
    else {
      const displayedSurveys = this.state.surveys;
      this.setState({ displayedSurveys });
    }
    
  }

  render() {
    const show = this.state.questions ? "hide" : "show";
    return (
        <div className="App">
          <header className="App-header">
            <img src={logo} className="App-logo" alt="logo" />
            <h1 className="App-title">IWD Challenge</h1>
          </header>
          <div id="body">
            <div className="">
              <h1> All surveys </h1>
              <input type="text" placeholder="Search a survey" onChange={(e) => this.filterSurveys(e.target.value)} />
            </div>
            <SurveyList active={this.state.activeQuestion} surveys={this.state.displayedSurveys} onClick={ (code) => this.getQuestions(code) } />
            <QuestionList className={show} questions={this.state.questions} />
          </div>
        </div>
    );
  }
}

export default App;
