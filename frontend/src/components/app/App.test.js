import axios from 'axios';
import moxios from 'moxios';
import sinon from 'sinon';
import { equal } from 'assert'
import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';


describe('mocking axios requests', function () {

  beforeEach(function () {
    // import and pass your custom axios instance to this method
    moxios.install()
  })

  afterEach(function () {
    // import and pass your custom axios instance to this method
    moxios.uninstall()
  })

  it('just for a single spec', function (done) {
    const div = document.createElement('div');
    ReactDOM.render(<App />, div);
    moxios.wait(function () {
      let request = moxios.requests.mostRecent()
      request.respondWith({
        status: 200,
        response: [
          {
            "name": "Chartres",
            "code": "XX2"
          },
          {
            "name": "Paris",
            "code": "XX1"
          },
          {
            "name": "Melun",
            "code": "XX3"
          }]
      }).then(function () {
        const nbChild = div.querySelector('.SurveyList-body').childElementCount;
        equal(nbChild, 3);
        const firstChild = div.querySelector('.SurveyList-body').firstChild;
        equal(firstChild.firstChild.innerHTML, 'Chartres');
        equal(firstChild.childNodes[1].innerHTML, 'XX2');
        firstChild.click();
        moxios.wait(function () {
          let request = moxios.requests.mostRecent()
          request.respondWith({
            status: 200,
            response: {
              qcm: [{"type":"qcm","label":"What best sellers are available in your store?","options":["Product 1","Product 2","Product 3"],"answer":[true,false,true]}],
              numeric:[{"type":"numeric","label":"Number of products?","answer":6200}],
              date: [{"type":"date","label":"What is the visit date?","answer":"2017-10-25T12:04:50.000Z"}]
            }
          }).then(function () {
            const nbQuestions = div.getElementsByClassName('QuestionList-row').length;
            equal(nbQuestions, 3);
            console.log(nbQuestions);
            done();
          })
        })
        done();
      })
    })
  })

})