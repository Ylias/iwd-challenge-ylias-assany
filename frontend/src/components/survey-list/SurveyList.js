import React, { Component } from 'react';
import './SurveyList.css';

class SurveyList extends Component {

    renderSurveys() {
        if(this.props.surveys) {
            return (
                <div className="SurveyList-body">
                {this.props.surveys.map((survey) =>
                    <div className={ this.props.active === survey.code ? "SurveyList-row active" : "SurveyList-row"} 
                    key={survey.code} 
                    onClick={() =>this.props.onClick(survey.code)}>
                        <div className="SurveyList-item">
                            {survey.name}
                        </div>
                        <div className="SurveyList-item">
                            {survey.code}
                        </div>
                    </div>
                )}
            </div>
            );
        }
        else {
            return (
                <div className="SurveyList-body">
                </div>
            );
        }
       

    }

    render() {
        return (
            <div className="SurveyList">
                <div className="SurveyList-header">
                    <div className="SurveyList-item">
                        Name
                    </div>
                    <div className="SurveyList-item">
                        Code
                    </div>
                </div>
                {this.renderSurveys()}
                
            </div>
        );
    }
}

export default SurveyList;
