import React from 'react';
import ReactDOM from 'react-dom';
import SurveyList from './SurveyList';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<SurveyList />, div);
});
