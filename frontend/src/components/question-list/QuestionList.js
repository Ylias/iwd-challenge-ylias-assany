import React, { Component } from 'react';
import './QuestionList.css';

class QuestionList extends Component {

    renderQuestions() {
        if(this.props.questions) {
            return (
                <div className="showing">
                    <div className="QuestionList-header">
                        <div className="QuestionList-item">
                            QCM
                        </div>
                    </div>
                    <div className="QuestionList-body">
                        {this.props.questions.qcm.map((question, i) =>
                            { return this.renderQcm(question, i)}
                        )}               
                    </div>
                    <div className="QuestionList-header">
                        <div className="QuestionList-item">
                            Numeric
                        </div>
                    </div>
                    <div className="QuestionList-body">
                        {this.props.questions.qcm.map((question, i) =>
                            { return this.renderNumeric(question, i)}
                        )}                  
                    </div>
                    <div className="QuestionList-header">
                        <div className="QuestionList-item">
                            Date
                        </div>
                    </div>
                    <div className="QuestionList-body">
                        {this.props.questions.qcm.map((question, i) =>
                            { return this.renderDate(question, i)}
                        )}                    
                    </div>
                </div>
            );
        }
        else {
            return (
                <div className="QuestionList-body">
                </div>
            );
        }
    }

    renderQcm(question, i) {
        return (
            <div className="QuestionList-row" key={i}>
                <div className="QuestionList-item">
                    {question.label}
                </div>
                <div className="QuestionList-item">
                    {question.options.map((option,j) => 
                        <div className="QuestionList-item-option"  key={j}>
                            <input type="checkbox" />
                            <label>{option}</label>
                        </div>
                    )}
                </div>                       
            </div>
        );
    }
    
    renderNumeric(question, i) {
        return (
            <div className="QuestionList-row" key={i}>
                <div className="QuestionList-item">
                    {question.label}
                </div> 
                <div className="QuestionList-item">
                    <input type="number" />
                </div>                       
            </div>
        );
    }
    
    renderDate(question, i) {
        return (
            <div className="QuestionList-row" key={i}>
                <div className="QuestionList-item">
                    {question.label}
                </div>
                <div className="QuestionList-item">
                    <input type="date" />
                </div>             
            </div>
                
        );
    }

    render() {
        return (
            <div className="QuestionList">
                {this.renderQuestions()}                
            </div>
        );
    }
}

export default QuestionList;
