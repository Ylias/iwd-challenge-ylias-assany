<?php

use services\SurveyService;

namespace test;
define('ROOT_PATH', realpath('.'));

require_once 'vendor/autoload.php';
require_once ROOT_PATH.'/src/services/SurveyService.php';

use services\SurveyService;

class SurveyServiceTest extends \PHPUnit\Framework\TestCase
{
    public function testGetAllSurveysReturn()
    {

        $datas = SurveyService::getAllSurveysWithoutQuestions();
        $this->assertEquals(count($datas), 3);
    }

    public function testGetSurveyReturn()
    {

        $datas = SurveyService::getSurveyByCode("XX3");
        $this->assertEquals(count($datas->getQcm()), 4);
        $this->assertEquals(count($datas->getNumeric()), 4);
        $this->assertEquals(count($datas->getDate()), 4);
    }
}
