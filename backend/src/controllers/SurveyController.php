<?php

namespace controllers {
    
    require_once ROOT_PATH.'/src/services/SurveyService.php';

    use Silex\Application;
    use Symfony\Component\HttpFoundation\Request;
    use services\SurveyService;
    use \models\Question;
    


    class SurveyController {

        public function getAllSurveys(Request $request, Application $app)
        {
            $surveys = SurveyService::getAllSurveysWithoutQuestions();
            $result = [];
            foreach ($surveys as $key => $value) {
                array_push($result,$value->jsonSerialize());
            }
            return $app->json($result);
        }

        public function getQuestionsFromCode(Request $request, Application $app, $code) {
            $survey = SurveyService::getSurveyByCode($code)->jsonSerialize();
            $result = [
                "qcm" => $survey["qcm"],
                "numeric" => $survey["numeric"],
                "date" => $survey["date"]
            ];
            return $app->json($result);
        }
    }
}
