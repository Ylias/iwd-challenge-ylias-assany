<?php

namespace services {

    class FileService {

        private static $path = ROOT_PATH.'/data/';
        
        static function getAllDatas() {
            $files = array_diff(scandir(self::$path), array('.', '..'));
            $result = [];
            foreach ($files as $key => $value) {
                $str = file_get_contents(self::$path.$value);
                $json = json_decode($str, true);
                array_push($result,$json);
            }
            return $result;
        }
    }
}

