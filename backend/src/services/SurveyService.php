<?php
namespace services {

    require_once 'FileService.php';
    require_once ROOT_PATH.'/src/models/Survey.php';
    require_once ROOT_PATH.'/src/models/Question.php';

    use \models\Question; 
    use \models\Survey;

    class SurveyService {

        static function getAllSurveysWithoutQuestions() {
            $surveys = [];
            $insert = true;
            $rawDatas = FileService::getAllDatas();
            foreach ($rawDatas as $entryKey => $entry) {
                $insert = true;
                foreach($surveys as $key => $value) {
                    if( $value->getCode() === $entry['survey']['code']) {
                        $insert = false;
                    }
                }
                if ($insert) {
                    $survey = new Survey(
                        $entry['survey']['name'],
                        $entry['survey']['code']
                    );
                    array_push($surveys,$survey);
                }
                
            }
            return $surveys;
        }

        static function getSurveyByCode($code) {
            $rawDatas = FileService::getAllDatas();
            $qcm = [];
            $numeric = [];
            $date = [];
            $survey = null;
            foreach ($rawDatas as $entryKey => $entry) {
                if ($code === $entry['survey']['code']) {
                    if ($survey === null) {
                        $survey = new Survey(
                            $entry['survey']['name'],
                            $entry['survey']['code'],
                            $qcm,
                            $numeric,
                            $date
                        );
                    }
                    foreach ($entry['questions'] as $questionKey => $questionEntry) {
                        $question = new Question(
                            $questionEntry['type'],
                            $questionEntry['label'],
                            $questionEntry['options'],
                            $questionEntry['answer']
                        );
                        switch ($question->getType()) {
                            case 'qcm':
                                array_push($qcm,$question);
                                break;
                            case 'numeric':
                                array_push($numeric,$question);
                                break;
                            case 'date':
                                array_push($date,$question);
                                break;
                        }
                    }
                }
            }
            $survey->setQcm($qcm);
            $survey->setNumeric($numeric);
            $survey->setdate($date);
            return $survey;
        }
    }
}

