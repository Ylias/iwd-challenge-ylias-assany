<?php

namespace models {

    require_once 'Question.php';

    class Survey extends Model{
        private $name;
        private $code;
        private $qcm;
        private $numeric;
        private $date;

        function __construct (){
            $numberParams = func_num_args();
            $parameters = func_get_args();
            switch ($numberParams) {
                case 2:
                    $this->constructWithoutQuestions(...$parameters);
                    break;
                case 5:
                    $this->constructWithQuestions(...$parameters);
                    break;
            }
        }

        function constructWithQuestions ($name, $code, $qcm,  $numeric, $date){
            $this->setName($name);
            $this->setCode($code);
            $this->setQcm($qcm);
            $this->setNumeric($numeric);
            $this->setDate($date);
        }

        function constructWithoutQuestions ($name, $code){
            $this->name = $name;
            $this->code = $code;
        }

        public function getName() {
            return $this->name;
        }

        public function getCode() {
            return $this->code;
        }

        public function getQcm() {
            return $this->qcm;
        }

        public function getNumeric() {
            return $this->numeric;
        }

        public function getDate() {
            return $this->date;
        }

        public function setName($name) {
            $this->name = $name;
        }

        public function setCode($code) {
            $this->code = $code;
        }

        public function setQcm($qcm) {
            $this->qcm = $qcm;
        }

        public function setNumeric($numeric) {
            $this->numeric = $numeric;
        }

        public function setDate($date) {
            $this->date = $date;
        }
    }
}
