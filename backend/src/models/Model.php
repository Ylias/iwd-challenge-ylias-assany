<?php

namespace models {

    use ReflectionClass;
    use ReflectionProperty;
    use JsonSerializable;

    class Model implements JsonSerializable {
        
        public function jsonSerialize() {
            $properties = get_class_vars(get_class($this));
            $getters = get_class_methods(get_class($this));
            $result = [];
            $reflection = new ReflectionClass($this);
            $properties = $reflection->getProperties(ReflectionProperty::IS_PRIVATE);
            foreach ($properties as $propertyKey => $propertyValue) {
                $propertyName = $propertyValue->name;
                $getterName = 'get'.ucfirst($propertyName);
                foreach ($getters as $getterKey => $getterValue) {
                    if($getterName === $getterValue) {
                        $getterResult = $this->$getterValue();
                        if(is_array($getterResult)){
                            $toAdd = [];
                            foreach ($getterResult as $key => $value) {
                                if (method_exists($value, 'jsonSerialize')) {
                                    array_push ( $toAdd, $value->jsonSerialize());
                                } else {
                                    array_push ( $toAdd, $value );
                                    
                                }
                            }
                        } elseif (method_exists($getterResult, 'jsonSerialize')) {
                            $toAdd = $getterResult->jsonSerialize();
                        } else {
                            $toAdd = $getterResult;
                        }
                        if ($toAdd !== null) {
                            $result[$propertyName] = $toAdd;                            
                        }                        
                    }
                }
            }            
            return $result;
        }
    }
}
