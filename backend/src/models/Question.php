<?php

namespace models{

    require_once 'Model.php';

    class Question extends Model{
        private $type;
        private $label;
        private $options;
        private $answer;

        function __construct (String $type,String $label,$options, $answer) {
            $this->setType($type);
            $this->setLabel($label);
            $this->setOptions($options);
            $this->setAnswer($answer);
        }

        function getType() {
            return $this->type;
        }

        function getLabel() {
            return $this->label;
        }

        function getOptions() {
            return $this->options;
        }

        function getAnswer() {
            return $this->answer;
        }

        function setType(String $type) {
            $this->type = $type;
        }

        function setLabel(String $label) {
            $this->label = $label;
        }

        function setOptions($options) {
            if($options === null || gettype($options) === 'array') {
                $this->options = $options;            
            }
        }

        function setAnswer($answer) {
            $type = gettype($answer);
            switch ($this->type) {
                case 'qcm' :
                    if ( strcmp($type,'array') === 0 ) {
                        $this->answer = $answer;              
                    }
                    break;
                case 'numeric' :
                    if ( strcmp($type,'integer') === 0 ) {
                        $this->answer = $answer;              
                    }
                    break;
                case 'date' :
                    if ( strcmp($type,'string') === 0 ) {
                        $this->answer = $answer;              
                    }
                    break;

            }
        }
    }
}
